<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
use Zend\Servicemanager\AbstractPluginManager;
use Zend\ServiceManager\ServiceLocatorInterface;
use Application\Session;
return array(
    'router' => array(
        'routes' => array(
            'pdf' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/gen/pdf[/]',
                    'defaults' => array(
                        'controller'    => 'generator',
                        'action'        => 'pdf',
                    ),
                ),
                'may_terminate' => true,
            ),
            'html' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/gen/html[/]',
                    'defaults' => array(
                        'controller'    => 'generator',
                        'action'        => 'html',
                    ),
                ),
                'may_terminate' => true,
            ),
            'test' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/gen/test[/]',
                    'defaults' => array(
                        'controller'    => 'generator',
                        'action'        => 'test',
                    ),
                ),
                'may_terminate' => true,
            ),
        ),
    ),
    'controllers' => array(
    'invokables' => array(
        'generator' => 'ResumeGenerator\Controller\GeneratorController',
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            'layout/pdf'           => __DIR__ . '/../view/resume-generator/generator/pdf.phtml',
            'layout/test'          => __DIR__ . '/../view/resume-generator/generator/test.phtml'
        ),
      'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);
