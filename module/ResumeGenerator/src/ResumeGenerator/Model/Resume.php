<?php
/**
 * ZfTable ( Module for Zend Framework 2)
 *
 * @copyright Copyright (c) 2013 Piotr Duda dudapiotrek@gmail.com
 * @license   MIT License
 */

namespace ResumeGenerator\Model;

use Zend\Db\Sql\Predicate\Expression;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;

use Application\Entity\Resume as ResumeEntity;

class Resume extends AbstractTableGateway {

    protected $table = 'resume';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new ResumeEntity($adapter));

        $this->initialize();
    }

    public function select($where = null)
    {
        if (!$this->isInitialized) {
            $this->initialize();
        }

        $select = $this->sql->select();

        if ($where instanceof \Closure) {
            $where($select);
        } elseif ($where !== null) {
            $select->where($where);
        }

        $this->resultSetPrototype->initialize($this->selectWith($select));

        return \Zend\Stdlib\ArrayUtils::iteratorToArray($this->resultSetPrototype,false);
    }

    public function selectMostRecentResume(){

        if (!$this->isInitialized) {
            $this->initialize();
        }

        $select = $this->sql->select()->columns(array('serialized_data'))->order('id DESC')->limit(1);

        $this->resultSetPrototype->initialize($this->selectWith($select));

        return \Zend\Stdlib\ArrayUtils::iteratorToArray($this->resultSetPrototype,false);
    }
}
