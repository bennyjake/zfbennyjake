<?php
namespace Admin\Session;

class SessionHelper extends \Zend\View\Helper\AbstractHelper
{
    protected $sessionService;

    public function setSessionService(
        $sessionService
    ) {
        $this->sessionService = $sessionService;
    }

    public function __invoke() {
        return $this->sessionService;
    }
}