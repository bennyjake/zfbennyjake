<?php

namespace Admin\Session;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
/**
 * Created by PhpStorm.
 * User: bchrisman
 * Date: 7/17/14
 * Time: 3:14 PM
 */

class SessionService
{
    protected $sessionContainer;

    public function setSessionContainer($sessionContainer){
        $this->sessionContainer = $sessionContainer;
    }

    public function set(array $array){
        foreach($array as $name => $value){
            if((!isset($name) || empty($name)) && !(isset($value))){
                $this->sessionContainer->getManager->getStorage()->clear('payroll');
            }
            else{
                $this->sessionContainer->{$name} = $value;
            }
        }
    }

    public function get(){
        return $this->sessionContainer->getArrayCopy();
    }

    public function __invoke() {
        return $this->sessionContainer;
    }
}