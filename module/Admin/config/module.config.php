<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
use Zend\Servicemanager\AbstractPluginManager;
use Zend\ServiceManager\ServiceLocatorInterface;
use Application\Session;
return array(
    'router' => array(
        'routes' => array(
            'zfcadmin' => array(
                'type' => 'literal',
                'options' => array(
                    'route'    => '/admin',
                    'defaults' => array(
                        'controller' => 'admin',
                        'action'     => 'index',
                    ),
                ),
                'may_terminate' => true,
            ),
            'dashboard' => array(
                'type' => 'literal',
                'options' => array(
                    'route'    => '/admin/dashboard',
                    'defaults' => array(
                        'controller' => 'admin',
                        'action'     => 'dashboard',
                    ),
                ),
                'may_terminate' => true,
            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            //http://stackoverflow.com/questions/20456500/zf2-get-global-session-container
            'sessionService' => function(ServiceLocatorInterface $serviceLocator) {
                $sessionContainer = new \Zend\Session\Container('payroll');
                $sessionService = new \Admin\Session\SessionService();
                $sessionService->setSessionContainer($sessionContainer);
                return $sessionService;
            },
            'TableUsers' => function($sm){
                $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                $userTable = new \Admin\Model\TableUsers($dbAdapter);
            },
            'PayrollUser' => function($sm){
                $userTable = $sm->get('TableUsers');
                $userSession = $sm->get('sessionService');
                $PayrollUser = new \Admin\Entity\User();
                $PayrollUser->setUserSession($userSession);
                $PayrollUser->setUserTable($userTable);
                $PayrollUser->setUserPost($sm->get('Request')->getPost()->getArrayCopy());
                return $PayrollUser;
            },
        ),
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'controller_plugins' => array(
        'factories' => array(
            'SessionPlugin' => function(AbstractPluginManager $pluginManager){
                    $sessionService = $pluginManager->getServiceLocator()->get('sessionService');
                    $sessionPlugin = new \Admin\Session\SessionPlugin();
                    $sessionPlugin->setSessionService($sessionService);
                    return $sessionPlugin;
                },
        ),
    ),
    'view_helpers' => array(
        'factories' => array(
            'SessionHelper' => function (AbstractPluginManager $pluginManager){
                    $sessionService = $pluginManager->getServiceLocator()->get('SessionService');
                    $sessionHelper = new \Admin\Session\SessionHelper();
                    $sessionHelper->setSessionService($sessionService);
                    return $sessionHelper;
                },
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'admin' => 'Admin\Controller\DashboardController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/admin'          => __DIR__ . '/../view/layout/admin.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
