<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        $config = $this->getServiceLocator()->get('Config');

        $form = new \Application\Form\LogInForm();

        $request = $this->getRequest();

        if ($request->isPost()) {

            $form->setData($this->params()->fromPost());
            if ($form->isValid()) {

                $form->getData();

                // Redirect to list of albums
                return $this->redirect()->toRoute('zfcadmin');
            }
        }

        return array('form' => $form,
            'name' => $config['name'],
            'phone' => $config['phone'],
            'email' => $config['email']);
    }
}