<?php

namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class LogInForm extends Form
{
    public function __construct(){

        parent::__construct('log-in');
        $this->setAttribute('method', 'post');

        $this->add(array(
                'name' => 'email',
                'type' => 'Zend\Form\Element\Text',
                'options' => array(
                    'label' => 'Email'
                ),
                'attributes' => array(
                    'required' => 'true'
                )
        ));

        $this->add(array(
                'name' => 'pass',
                'type' => 'Zend\Form\Element\Password',
                'options' => array(
                    'label' => 'Password'
                ),
                'attributes' => array(
                    'required' => 'true'
                )
        ));

        //prevent automated script submission
        /*$this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'attributes' => array(
                 'required' => 'false'
            )
        ));*/

        //Submit Button - 'Save'
        $this->add(array(
                'name' => 'submit',
                'type' => 'Zend\Form\Element\Submit',
                'attributes' => array(
                    'value' => 'Log In',
                    'required' => 'false'
                )
        ));
    }
}